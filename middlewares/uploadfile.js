const multer = require('koa-multer')
const path = require('path')

function mkdir(){
  let root = path.join(__dirname, '../public/upload');
  return root;
}

const update = () => {
  let storage = multer.diskStorage({
    //文件保存路径
      destination: function (req, file, cb) {
        let path = mkdir()
        cb(null, path) //注意路径必须存在
      },
      //修改文件名称
      filename: function (req, file, cb) {
        //获取后缀名
        var fileFormat = (file.originalname).split(".");
        cb(null, Date.now() + "." + fileFormat[fileFormat.length - 1]);
      }
  })

  const limits = {
    fields: 10,//非文件字段的数量
    fileSize: 500 * 1024,//文件大小 单位 b
    files: 1//文件数量
  }

  return multer({ storage, limits })
}

module.exports = {
  update
}