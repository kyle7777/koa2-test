/**
 * @description: 登录验证的中间件
 */

 const { loginCheckFailInfo } = require('../model/ErrorInfo')
 const { ErrorModel } = require('../model/ResModel')
 
 /**
  * @param {Object} ctx
  * @param {Function} next
  * @description: API 登录验证
  */
 async function loginCheck(ctx, next) {
   if (ctx.session && ctx.session.userInfo) {
     // 已登录
     await next()
     return
   }
   // 未登录
   ctx.body = new ErrorModel(loginCheckFailInfo)
 }
 /**
  * @param {Object} ctx
  * @param {Function} next
  * @description: 页面登录验证
  */
 async function loginRedirect(ctx, next) {
   if (ctx.session && ctx.session.userInfo) {
     // 已登录
     await next()
     return
   }
   // 未登录
   const curUrl = ctx.url
   ctx.redirect('/login?url=' + encodeURIComponent(curUrl))
 }
 
 module.exports = {
   loginCheck,
   loginRedirect
 }
 