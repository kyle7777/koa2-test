/**
 *  基础模型 
 */
class BaseModel {
  constructor({ error, data, message }) {
    this.error = error
    this.data = data
    this.message = message
  }
}

class SuccessModel extends BaseModel {
  constructor(data = {}) {
    super({
      error: 0,
      data,
      message: null
    })
  }
}

class ErrorModel extends BaseModel {
  constructor({ error, message }) {
    super({
      error,
      message,
      data: null
    })
  }
}


module.exports = {
  SuccessModel,
  ErrorModel
}