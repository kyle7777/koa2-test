const queryUserFailInfo = {
  error: 10036,
  message: '获取用户失败'
}

const userFailInfo = {
  error: 10038,
  message: '用户名或密码错误'
}

const addUserFailInfo = {
  error: 10039,
  message: '新增用户失败'
}

const updateUserFailInfo = {
  error: 10040,
  message: '修改用户失败'
}

const deleteFailInfo = {
  error: 10041,
  message: '删除用户失败'
}

const registerUserNameExistInfo = {
  error: 10042,
  message: '用户名已存在'
}

const loginCheckFailInfo = {
  error: 10043,
  message: '您尚未登录'
}

const updateFileFailInfo = {
  erron: 100044,
  message: '上传文件出错'
}

module.exports = {
  queryUserFailInfo,
  userFailInfo,
  addUserFailInfo,
  updateUserFailInfo,
  deleteFailInfo,
  registerUserNameExistInfo,
  loginCheckFailInfo,
  updateFileFailInfo
}