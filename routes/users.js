const router = require('koa-router')()
// const users = require('../db/model/user')
const { findUser, loginUser, addUser, updateUser } = require('../controller/api/user')
// const { loginCheck } = require('../middlewares/loginCheck')
const jwt = require('jsonwebtoken')
const util = require('util')
const { SECRET } = require('../conf')
const verify = util.promisify(jwt.verify)

router.prefix('/api/users')

router.post('/', async (ctx, next) => {
  ctx.body = await findUser()
})

router.post('/login', async (ctx, next) => {
  const { username, password } = ctx.request.body

  ctx.body = await loginUser({
    ctx,
    username,
    password
  })
})

router.post('/add', async (ctx, next) => {
  const { username, password, sex, age, avatar } = ctx.request.body

  ctx.body = await addUser({
    username,
    password,
    sex,
    age,
    avatar
  })
})

router.post('/update', async (ctx, next) => {
  const { _id, username, password, sex, age, avatar } = ctx.request.body

  ctx.body = await updateUser({
    _id,
    username,
    password,
    sex,
    age,
    avatar
  })
})

router.post('/delete', async (ctx, next) => {
  const { _id } = ctx.request.body

  ctx.body = await deleteUser({ _id })
})

// 获取用户信息
router.get('/getUserInfo', async function (ctx, next) {
  const token = ctx.header.authorization
  console.log(token, 'token')

  try {
    const payload = await verify(token.split(' ')[1], SECRET)
    ctx.body = {
      errno: 0,
      userInfo: payload
    }
  } catch (ex) {
    ctx.body = {
      errno: 0,
      msg: 'verify token failed'
    }
  }
})


module.exports = router
