const router = require('koa-router')()

router.prefix('/api/task')

router.post('/add', async (ctx, next) => {
  await ctx.render('index', {
    title: 'Hello task!'
  })
})

module.exports = router
