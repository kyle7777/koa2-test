const router = require('koa-router')()
const fs = require('fs')
const { update } = require('../middlewares/uploadfile')
const { updateFile } = require('../controller/common/upload')

router.prefix('/api/upload')

router.post('/img', update().single('file'), async (ctx, next) => {
  const file = ctx.req.file
  ctx.body = await updateFile(file.filename)
})

module.exports = router
