const { SuccessModel, ErrorModel } = require('../../model/ResModel.js')
const users = require('../../db/model/user.js')
const  { queryUserFailInfo, userFailInfo, addUserFailInfo, registerUserNameExistInfo, updateUserFailInfo, deleteFailInfo } = require('../../model/ErrorInfo.js')
const { getUserInfo } = require('../../services/user')
const { doCrypto } = require('../../utils/cryp')
const jwt = require('jsonwebtoken')
const { SECRET } = require('../../conf')

// 查询用户
const findUser = async (params) => {
  const result = await users.find()
  console.log(result, 'result')
  if (result) {
    const newResult = result.map(curr => {
      const { username, sex, age, avatar } = curr
      return { username, sex, age, avatar }
    })
    return new SuccessModel(newResult)
  } else {
    return new ErrorModel(queryUserFailInfo)
  }
}

// 登录
const loginUser = async (params) => {
  const { ctx, username, password, sex, age, avatar } = params
  const userInfo = await getUserInfo(username, doCrypto(password))
  
  if (!userInfo) {
    return new ErrorModel(userFailInfo)
  }

  // 登录成功
  let token = jwt.sign(userInfo, SECRET, { expiresIn: '1h' })

  return new SuccessModel(token)
}

// 新增
const addUser = async (params) => {
  const { username, password, sex, age, avatar } = params
  const userInfo = await getUserInfo(username)

  if (userInfo) {
    return new ErrorModel(registerUserNameExistInfo)
  }

  const result = await users.create({
    username,
    password: doCrypto(password),
    sex,
    age,
    avatar
  })

  if (result) {
    const { username, sex, age, avatar, _id } = result
    return new SuccessModel({
      username,
      sex,
      age,
      avatar,
      _id    
    })
  } else {
    return new ErrorModel(addUserFailInfo)
  }
}

// 修改用户
const updateUser = async (params) => {
  const { username, password, sex, age, avatar } = params

  const result = await users.update({
    _id
  }, 
  { 
    username,
    password,
    sex,
    age,
    avatar
  })

  if (result) {
    const { username, sex, age, avatar, _id } = result
    return new SuccessModel({
      username,
      sex,
      age,
      avatar,
      _id    
    })
  } else {
    return new ErrorModel(updateUserFailInfo, deleteFailInfo)
  }
}

// 删除用户
const deleteUser = async (params) => {
  const { _id } = params

  const result = await users.deleteOne({ _id });

  if (result) {
    return new SuccessModel({
      message: '删除用户成功'    
    })
  } else {
    return new ErrorModel(deleteFailInfo)
  }
}

module.exports = {
  findUser,
  loginUser,
  addUser,
  updateUser,
  deleteUser
}