const { SuccessModel, ErrorModel } = require('../../model/ResModel.js')
const  { updateFileFailInfo } = require('../../model/ErrorInfo.js')

// 上传文件
const updateFile = async (filename) => {
  try {
      return new SuccessModel({
          name: filename,
          url: 'http://localhost:3000/upload/' + filename
      });
  }
  catch (err) {
      return new ErrorModel(updateFileFailInfo);
  }
}

module.exports = {
  updateFile
}