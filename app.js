const Koa = require('koa')
const cors = require('koa2-cors')
const Session = require('koa-generic-session')
const { SECRET } = require('./conf')
const koajwt = require('koa-jwt')
const app = new Koa()


const views = require('koa-views')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')

const { logger, accessLogger } = require('./logger.js');
// const logger = require('koa-logger')
const mongoConf = require('./db/config')
mongoConf.connect()

const FilterRoutes = require('./filters/routes')
const index = require('./routes/index')
const users = require('./routes/users')
const tasks = require('./routes/task')
const uploads = require('./routes/upload')

// error handler
onerror(app)

app.use(
  cors({
      origin: function(ctx) { //设置允许来自指定域名请求
          // if (ctx.url === '/test') {
          //     return '*'; // 允许来自所有域名请求
          // }
          return 'http://192.168.31.197:8080' //只允许http://localhost:8080这个域名的请求
        // return '*';
      },
      maxAge: 5, //指定本次预检请求的有效期，单位为秒。
      credentials: true, //是否允许发送Cookie
      allowMethods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'], //设置所允许的HTTP请求方法
      allowHeaders: ['Content-Type', 'Authorization', 'Accept', 'X-Requested-With'], //设置服务器支持的所有头信息字段
      exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'] //设置获取其他自定义字段
  })
)

// 1. 错误信息处理
app.use(async (ctx, next) => {
  return await next().catch((err) => {
    if (401 == err.status) {
      ctx.body = {
        code: 50001,
        message: '用户鉴权失败'
      }
    } else {
      throw err
    }
  })
})

app.use(koajwt({ secret: SECRET }).unless({
    // 登录接口不需要验证
  path: FilterRoutes
}))

//配置session
// app.keys = [COOKIE_KEY];
// app.use(Session({
//     key: 'test:sid',
//     prefix: 'test:sess:',
//     rolling: true, /** 是否每次响应时刷新Session的有效期。(默认是 false) */
//     cookie: {
//         path: '/',
//         httpOnly: true,  // 客户端不能修改cookie
//         maxAge: 24 * 60 * 60 * 1000 // cookie过期时间，ms，自动配置redis过期时间
//     }
// }))

// middlewares
app.use(bodyparser({
  enableTypes: ['json', 'form', 'text'],
}))
// app.use(koaBody({
//   multipart: true  //  ***** 就是这个 (是否支持 multipart-formdate 的表单)
// }))

app.use(json())
// app.use(logger())
app.use(accessLogger())
app.use(require('koa-static')(__dirname + '/public'))

app.use(views(__dirname + '/views', {
  extension: 'pug'
}))

// logger
// app.use(async (ctx, next) => {
//   const start = new Date()
//   await next()
//   const ms = new Date() - start
//   console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
// })

app.use(async (ctx, next) => {
  const start = new Date()
  await next()
  const ms = new Date() - start
  console.log(ctx.request.headers['x-forwarded-for'], 'ip')
  logger.info(`${ctx.method} ${ctx.request.ip} ${ctx.url} - ${ms}ms`)
})

// routes
app.use(index.routes(), index.allowedMethods())
app.use(users.routes(), users.allowedMethods())
app.use(tasks.routes(), tasks.allowedMethods())
app.use(uploads.routes(), uploads.allowedMethods())

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
});

module.exports = app
