const mongoose = require('mongoose');
const Schema = mongoose.Schema

// Schema
const usersSchema = new Schema({
  create_user_id: { type: String }, // 创建人id
  task_name: { type: String, required: [true, '任务名称不能为空'] },
  create_time: { type: String, default: Date.now }, // 创建时间
  update_time: { type: String }, // 更新时间
  stop_time: { type: String }, // 暂停时间
  is_delete: { type: String, enum: [1, 2] }, // 是否删除：1已删除，2未删除
  task_status: { type: String, enum: [1, 2, 3, 4] }, // 完成状态：1正在进行 2延期 3暂停 4完成
  remark: { type: String }, // 备注
  logo: { type: String }, // 任务图片
})

// Model
const tasks = mongoose.model('task', usersSchema, 'task');

module.exports = tasks;