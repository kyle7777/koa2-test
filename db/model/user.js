const mongoose = require('mongoose');
const Schema = mongoose.Schema

// Schema
const usersSchema = new Schema({
    username: { type: String, required: [true, 'username不能为空'] },
    password: { type: String, required: [true, 'password不能为空'] },
    sex: { type: String, enum: ['man','woman'], required: [true, 'sex不能为空'] },
    age: { type: Number, required: [true, 'age不能为空'] },
    avatar: { type: String, default: '' }
})

// Model
const users = mongoose.model('user', usersSchema, 'user');

module.exports = users;