const users = require('../db/model/user.js')

/**
 * @param {*} userName 用户名
 * @param {*} password 密码
 * @description: 获取用户的信息
 */
 async function getUserInfo(username, password) {
  const whereOpt = {
    username
  }
  if (password) {
    Object.assign(whereOpt, { password })
  }

  // 查询
  const result = await users.find(whereOpt)

  // 格式化
  if (result.length) {
    const { username, sex, age, _id } = result[0]

    return {
      username,
      sex,
      age,
      _id    
    }
  }

  return null
}

module.exports = {
  getUserInfo
}